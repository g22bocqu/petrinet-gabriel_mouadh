package abstraction;

public class OutEdge extends Edge{
	private Place place;
	private Transition transition;

	public OutEdge(int value, String id, Place place, Transition transition) throws NegativeNumberException {
		super(value, id);
		// TODO Auto-generated constructor stub
		this.place = place;
		this.transition= transition;
	}

	@Override
	public int getValue() {
		return super.getValue();
	}

	@Override
	public void setValue(int value) throws NegativeNumberException {
		super.setValue(value);
	}
	
	@Override
	public String getId() {
		return super.getId();
	}

	public Place getPlace() {
		return this.place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}
	
	public Transition getTransition() {
		return this.transition;
	}

	public void setTransition(Transition transition) {
		this.transition = transition;
	}

	/**
	 * @author Gabriel
	 * @Override
	 * Cette methode affiche un OutEdge
	 */
	public String toString() {
		String affichage ="";
		if(this.place !=null) {
			affichage +=super.toString() +" Place : " + this.place.getId();
		}
		else{
			affichage += super.toString();
		}
		if(transition !=null) {
			affichage += " Transition : " +transition.getId();
		}
		return affichage;
	}

	/**
	 * @author Gabriel
	 * @Override
	 * @param outEdge
	 * Deux outEdge sont egaux si et seulement si leur id et leur valeur sont egaux
	 */
	public boolean equals(Object outEdge) {
		if(outEdge !=null && outEdge instanceof OutEdge 
				&& this.getId().equals(((OutEdge)outEdge).getId())
				&& this.getValue() == ((OutEdge)outEdge).getValue()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Cette place permet de verifier que OutEdge va transferer des jetons
	 * @return true si place.token >= this.value
	 */
	public boolean isTriggerable() {
		if(this.place != null) {
			if(this.place.getToken() > this.getValue() - 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author Gabriel
	 * Cette methode effectue une transition si le nombre de jeton dans une place
	 * est plus grand que la valeur de l'edge
	 */
	public void trigger() {
		//Si jeton >= valeur on peut retirer valeur jetons de Place sinon ne fait rien
		if(this.isTriggerable()) {
			try {
				this.place.transit(-this.getValue());
			} catch (NegativeNumberException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
