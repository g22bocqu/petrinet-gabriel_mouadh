package abstraction;

public class Edge {
	private int value;
	private String id;

	public Edge(int value, String id) throws NegativeNumberException {
		// <=> value <=0
		if(value < 1 ) {
			throw new NegativeNumberException("Valeur d'entree negative ou null");
		}
		this.value = value;
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) throws NegativeNumberException {
		if(value <1 ) {
			throw new NegativeNumberException("Valeur d'entree negative ou null");
		}
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public String toString() {
		return "Id : "+this.getId()  + " Value: " + this.value;
	}

}
