/**
 * @author Gabriel
 * @since 19/10/2023
 * Ce package contient toutes les classes necessaires au fonctionnement de notre PetriNet (Place, Transition, Edge,...)
 */
package abstraction;