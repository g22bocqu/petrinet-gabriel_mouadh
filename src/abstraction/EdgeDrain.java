package abstraction;

public class EdgeDrain extends OutEdge{

	public EdgeDrain(String id, Place place, Transition transition) throws NegativeNumberException {
		//ici value n'a aucun interet et on l'utilisera pas.
		//Par default on la fixe a 1 puisque  le trigger se declenche si la place
		//a plus d'un jeton
		super(1, id, place, transition);	
	}

	@Override
	public int getValue() {
		return super.getValue();
	}

	@Override
	public String getId() {
		return super.getId();
	}
	@Override
	public Place getPlace() {
		return super.getPlace();
	}

	@Override
	public void setPlace(Place place) {
		super.setPlace(place);
	}

	@Override
	public Transition getTransition() {
		return super.getTransition();
	}

	@Override
	public void setTransition(Transition transition) {
		super.setTransition(transition);
	}

	/**
	 * @Override
	 * @author Gabriel
	 */
	public String toString() {
		return super.toString();
	}

	@Override
	public boolean equals(Object edgeDrain) {
		//Deux EdgeDrain sont egaux si et seulement si ils sont egaux au sens de OutEdge
		return super.equals(edgeDrain);
	}

	/**
	 * @Override 
	 * @author Gabriel
	 * Un EdgeDrain se declanche uniquement quand une place a plus d'un jeton
	 */
	public boolean isTriggerable() {
		if(this.getPlace() != null) {
			return this.getPlace().getToken() > 0;
		}
		return false;
	}

	/**
	 * @Override
	 * @author Gabriel
	 * Si les jetons sont superieurs a 1 vide la place, sinon on ne fait rien
	 */
	public void trigger() {
		if(this.isTriggerable()) {
			try {
				this.getPlace().transit(-this.getPlace().getToken());
			} catch (NegativeNumberException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
