package test;

import abstraction.EdgeDrain;
import abstraction.EdgeZero;
import abstraction.NegativeNumberException;
import abstraction.OutEdge;
import abstraction.Place;
import abstraction.Transition;
import junit.framework.TestCase;

public class TestEdgeDrain extends TestCase{

	/**
	 * @author Gabriel
	 * Ce test permet de verifier qu'un EdgeDrain va uniquement transiferer des jetons si le nombre de jeton dans
	 * place est > 1
	 */
	public void testIsTriggerable() {
		Place place;
		try {
			place = new Place(5, "p1");
			Place place1 = new Place(0,"p2");
			Transition transition = new Transition("t1");
			OutEdge edgeDrain1 = new EdgeDrain("ed1", place, transition);
			OutEdge edgeDrain2 = new EdgeDrain("ed2", place1, transition);
			OutEdge edgeDrain3 = new EdgeDrain("ed3", null, transition);
			assertTrue(edgeDrain1.isTriggerable());
			assertFalse(edgeDrain2.isTriggerable());
			assertFalse(edgeDrain3.isTriggerable());
		} catch (NegativeNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @Test
	 * @author Gabriel
	 * Cette methode verifie qu'on vide bien la place quand le nombre de jeton est sup a 1
	 */
	public void testTriggerJetonSup1() {
		Place place;
		try {
			place = new Place(40, "p1");
			Transition transition = new Transition("t1");
			OutEdge outEdge1 = new EdgeDrain("ed1", place, transition);
			outEdge1.trigger();
			//On verifie qu'il n'y a bien pas de transition
			assertEquals(0,place.getToken());		       
		} catch (NegativeNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Unexpected error");
		}
	}

	/**
	 * @Test
	 * @author Gabriel
	 * Cette methode verifie qu'il ne se passe rien si jeton = 0 
	 */
	public void testTriggerJetonInf1() {
		Place place;
		try {
			place = new Place(0, "p1");
			Transition transition = new Transition("t1");
			OutEdge outEdge1 = new EdgeDrain("ed1", place, transition);
			outEdge1.trigger();
			//On verifie qu'il n'y a bien pas de transition
			assertEquals(0,place.getToken());		       
		} catch (NegativeNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Unexpected error");
		}
	}
}
