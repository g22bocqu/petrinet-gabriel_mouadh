package test;

import abstraction.EdgeZero;
import abstraction.NegativeNumberException;
import abstraction.OutEdge;
import abstraction.Place;
import abstraction.Transition;
import junit.framework.TestCase;

public class TestEdgeZero extends TestCase{

	/**
	 * @author Gabriel
	 */
	public void testIsTriggerable() {
		Place place;
		try {
			place = new Place(0, "p1");
			Place place1 = new Place(1,"p2");
			Transition transition = new Transition("t1");
			OutEdge edgeZero1 = new EdgeZero("ez1", place, transition);
			OutEdge edgeZero2 = new EdgeZero( "ez2", place1, transition);
			OutEdge edgeZero3 = new EdgeZero("ez3", null, transition);
			assertTrue(edgeZero1.isTriggerable());
			assertFalse(edgeZero2.isTriggerable());
			assertFalse(edgeZero3.isTriggerable());
		} catch (NegativeNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testTrigger() {
		Place place;
		try {
			place = new Place(0, "p1");
			Transition transition = new Transition("t1");
			OutEdge outEdge1 = new EdgeZero("ez1", place, transition);
			outEdge1.trigger();
			//On verifie qu'il n'y a bien pas de transition
			assertEquals(0,place.getToken());		       
		} catch (NegativeNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Unexpected error");
		}
	}
}
