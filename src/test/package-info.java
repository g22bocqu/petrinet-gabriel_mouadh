/**
 * @author Gabriel
 * @since 19/10/2023
 * Ce package a la responsabilite de s'assurer du bon fonctionnement des methodes utilisees dans le package abstraction
 * NB : Les methodes se resummant a : return super.nomDeMethode() ne seront pas testees puisque la methode mere a 
 * deja ete teste
 */
package test;