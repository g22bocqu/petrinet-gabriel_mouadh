# PetriNet Gabriel_Mouadh

## Description
Ceci est l'implementation en Java des diagrammes de classe et de séquence déjà établis décrivant un réseau PetriNet.
Dans le dossier source, on trouve 2 packages : 
- abstraction qui contient les différentes classes et les méthodes implémentées
- test qui contient les différents tests utilisés pour vérifier la bonne implémentation des méthodes ( les tests sont regroupés par classe)

## Détails de l'environnement 
Pour ce projet, on utilise la Version: 2022-06 (4.24.0) d'Eclipse et le JRE 11.

## Usage
L'execution des différentes classes crées dans test permet de vérifier si le programme est bon ou pas. Nous avons créé une classe de test pour chaque classe. Quasiment toutes les méthodes sont testées (les seules qui ne le sont pas sont les méthodes qui se contente d'être redéfinie avec super.nomDeLaMethode()).

## Conformité avec les modèles de conception
On trouvera quelques différences entre les modèles de conception déposés et ce code, à priori, les noms des variables ont été changé pour qu'ils soient tous en anglais. On a jouté également une fonction isTriggerable dans la classe transition, pour bien garantir que la fonction step ne passe que si tous les arcs liés à cette transition sont bons.
Les classes transition et place contiennent désormais de nouveaux attributs : les listes des arcs entrant et sortant qui y sont associés (sous forme d'une HashMap)
On a ajouté également pour les classes transition et place des méthodes pour lier (add) ou enlever (remove) des arcs, et pour toutes les classes: des getters, des setters, des toString et equals afin de faciliter l'utilisation des autres méthodes.


## Auteurs
Gabriel Bocquet et Mouadh Bondka


***



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.imt-atlantique.fr/g22bocqu/petrinet-gabriel_mouadh.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.imt-atlantique.fr/g22bocqu/petrinet-gabriel_mouadh/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
